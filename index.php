<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP introduction</title>
</head>
<body>
    <?php
        //pour afficher les erreurs :
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    ?>
    <h1>Paramètres</h1>
    <h2>Exercice 1</h2>
    <!-- Faire une page index.php. 
    Tester sur cette page que tous les paramètres de cette URL existent et les afficher: 
    index.php?nom=Nemare&prenom=Jean -->
    <p>Cliquer sur ce lien pour afficher ses paramètres dans l'url :
        <a href= "index.php?nom=Nemare&prenom=Jean">Jean Nemare</a>
        </br>
        <?php 
            //afficher les paramètres :
            echo ("paramètre NOM :".$_GET['nom']."</br>");
            echo ("paramètre PRENOM :".$_GET['prenom']."</br>");
        ?>
    </p>
    <h2>Exercice 2</h2>
    <!-- Faire une page index.php. 
    Tester sur cette page que le paramètre age existe et si c'est le cas l'
    afficher sinon le signaler :_ index.php?nom=Nemare&prenom=Jean_ -->
    <p>Cliquer sur ce lien pour afficher ses paramètres dans l'url :
        <a href= "index.php?nom=Nemare&prenom=Jean">Jean Nemare</a>
        </br>
        <?php
        //vérifie si le paramètre age existe
            if (isset($_GET['age'])){
                echo $age;
            }else {
                echo "le paramètre AGE n'existe pas";
            };
        ?>
    </p>
    <h2>Exercice 3</h2>
    <!-- Faire une page index.php. 
    Tester sur cette page que tous les paramètres de cette URL existent et les afficher:
    index.php?dateDebut=2/05/2016&dateFin=27/11/2016 -->
    <p>Cliquer sur ce lien pour afficher ses paramètres dans l'url :
        <a href= "index.php?dateDebut=2/05/2016&dateFin=27/11/2016">Début-Fin</a>
        </br>
        <?php 
        function verif($param){
            if (isset($param) && strlen($param)>0){
                echo "le paramètre existe : ".$param ;
            }else{
                echo "le paramètre n'existe pas";
            }
        };
        echo "</br> Pour la date de début, ";
        echo verif($_GET['dateDebut']);
        echo "</br> Pour la date de fin, ";
        echo verif($_GET['dateFin']);
        ?>
    </p>
    <h2>Exercice 4</h2>
    <!-- Faire une page index.php. 
    Tester sur cette page que tous les paramètres de cette URL existent et les afficher:
    index.php?langage=PHP&serveur=LAMP -->
    <p>Cliquer sur ce lien pour afficher ses paramètres dans l'url :
        <a href= "index.php?langage=PHP&serveur=LAMP">langage/serveur</a>
        </br>
        <?php 
        echo "</br> Pour le langage, ";
        echo verif($_GET['langage']);
        echo "</br> Pour le serveur, ";
        echo verif($_GET['serveur']);
        ?>
    </p>
    <h2>Exercice 5</h2>
    <!-- Faire une page index.php. 
    Tester sur cette page que tous les paramètres de cette URL existent et les afficher: 
    index.php?semaine=12 -->
    <p>Cliquer sur ce lien pour afficher ses paramètres dans l'url :
        <a href= "index.php?semaine=12">semaine</a>
        </br>
        <?php 
        echo "</br> Pour la semaine, ";
        echo verif($_GET['semaine']);
        ?>
    </p>
    <h2>Exercice 6</h2>
    <!-- Faire une page index.php. 
    Tester sur cette page que tous les paramètres de cette URL existent et les afficher: 
    index.php?batiment=12&salle=101 -->
    <p>Cliquer sur ce lien pour afficher ses paramètres dans l'url :
        <a href= "index.php?batiment=12&salle=101">emplacement</a>
        </br>
        <?php 
        echo "</br> Pour le bâtiment, ";
        echo verif($_GET['batiment']);
        echo "</br> Pour la salle, ";
        echo verif($_GET['salle']);
        ?>
    </p>
    <h1>Formulaires</h1>
    <div>
        <h2>Exercice 5</h2>
    <!-- Exercice 5 Créer un formulaire sur la page index.php avec :

    Une liste déroulante pour la civilité (Mr ou Mme)
    Un champ texte pour le nom
    Un champ texte pour le prénom

    Ce formulaire doit rediriger vers la page index.php. 
    Vous avez le choix de la méthode. -->
        <form id ="form" method="GET" action="index.php">
            <label for="mrmme">Civilité : </label>
            <select name="civilite" id="civ" >
                <option value="Mr">Monsieur
                <option value="Mme">Madame
            </select>
            <label for="nom" >Prénom :</label>
            <input type="text" name="nom" placeholder="Nemare">
            <label for="prenom" >Nom :</label>
            <input type="text" name="prenom" placeholder="Jean">
            <input type="submit" name="OK">
        </form>
        <?php
        if (strlen($_GET['nom'])>0 && strlen($_GET['prenom'])>0){
            echo "la civilité saisie est : ". $_GET['civilite']. "</br>le nom saisi est : ". $_GET['nom']."</br>Le prénom saisi est : ". $_GET['prenom'];
        }else {
            echo "Les paramètres n'ont pas été correctement saisis";
        };
        ?>
    </div>
</body>
</html>

