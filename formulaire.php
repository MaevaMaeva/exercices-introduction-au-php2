<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP introdution</title>
</head>
<body>
    <?php
        //pour afficher les erreurs :
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        
        //Pour mettre des titres :
        function titre ($title){
            echo '<h2>EXERCICE '.$title.'</h2>';
        }

    ?>
    <main>
        <h1>Formulaires</h1>
        <div>
            <?php
                echo titre(1);
            ?>
            <!-- EXERCICE 1 : Créer un formulaire demandant le nom et le prénom. 
            Ce formulaire doit rediriger vers la page user.php avec la méthode GET. -->
            <form method="GET" action="user.php">
                <p>
                    <label for="nom" >Prénom :</label>
                    <input type="text" name="nomGet" placeholder="Nemare">
                    <label for="prenom" >Nom :</label>
                    <input type="text" name="prenomGet" placeholder="Jean">
                    <input type="submit" name="OK">
                </p>
            </form>
        </div>  
        <div>
            <?php
                echo titre(2);
            ?>
            <!-- EXERCICE 2 : Créer un formulaire demandant le nom et le prénom. 
            Ce formulaire doit rediriger vers la page user.php avec la méthode POST. -->
            <form method="POST" action="user.php">
                <p>
                    <label for="prenom" >Prénom :</label>
                    <input type="text" name="nomPost" placeholder="Nemare">
                    <label for="nom" >Nom :</label>
                    <input type="text" name="prenomPost" placeholder="Jean">
                    <input type="submit" name="OK">
                </p>
            </form>
        </div>  

    </main>
</body>
</html>
