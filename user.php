<?php
    //page résultat

    //pour afficher les erreurs :
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    //Pour mettre des titres :
    function titre ($title){
        echo '<h2>EXERCICE '.$title.'</h2>';
    }
    // Exercice 3 : Avec le formulaire de l'exercice 1, 
    // afficher dans la page user.php les données du formulaire transmis.
    echo titre(3);
    
    if (strlen($_GET['nomGet'])>0 && strlen($_GET['prenomGet'])>0){
        echo "le nom saisi est : ". $_GET['nomGet']."</br>Le prénom saisi est : ". $_GET['prenomGet'];
    }else {
        echo "Les paramètres n'ont pas été correctement saisis";
    };

    // Exercice 4 : Avec le formulaire de l'exercice 2, 
    // afficher dans la page user.php les données du formulaire transmises.
    echo titre(4);
    if (strlen($_POST['nomPost'])>0 && strlen($_POST['prenomPost'])>0){
        echo "le nom saisi est : ". $_POST['nomPost']."</br>Le prénom saisi est : ". $_POST['prenomPost'];
    }else {
        echo "Les paramètres n'ont pas été correctement saisis";
    };